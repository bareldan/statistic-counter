import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class OutputFile {

    private String outFile;

    private final DataHandler dataHandler;

    public OutputFile(String outFile, DataHandler dataHandler) { // constructor
        this.outFile = outFile; // set the output file
        this.dataHandler = dataHandler; // set the data handler
        String fileType = checkFileType(); // check the output file type/format
        if (fileType.equals("json")) { // will be json
            writeToJson();
        } else if (fileType.equals("xml")) { // will be xml
            writeToXml();
        }
    }


    private void writeToJson() {
        try {
            FileWriter fileWriter = new FileWriter(outFile);
            fileWriter.write(dataHandler.toJson());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error: " + outFile + " is not a valid file");
        }
    }

    private void writeToXml() { // write to xml
        try {
            FileWriter fileWriter = new FileWriter(outFile);
            fileWriter.write(dataHandler.toXml());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error: " + outFile + " is not a valid file");
        }
    }

    private String checkFileType() { // check if the file type is valid and not present from the beginning
        if (outFile.contains(".json")) {
            return "json";
        } else if (outFile.contains(".xml")) {
            return "xml";
        } else {
            while (true) { // take until the user enters a valid file type
                System.out.println("Error: " + outFile + " is not a valid file type");
                Scanner scanner = new Scanner(System.in);
                System.out.println("Please enter a valid file type (json/xml): ");
                String fileType = scanner.nextLine();
                if (fileType.equals("json")) {
                    outFile += ".json";
                    return "json";
                } else if (fileType.equals("xml")) {
                    outFile += ".xml";
                    return "xml";
                }
            }
        }
    }
}
