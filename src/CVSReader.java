
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CVSReader {

    private final DataHandler dataHandler;


    List<String[]> data = new ArrayList<String[]>();

    private final String cvsFile;

    public CVSReader(String cvsFile, DataHandler dataHandler) {
        this.cvsFile = cvsFile;
        this.dataHandler = dataHandler;
        OpenFile();
        dataHandler.getInfo();
    }

    private void OpenFile() { // try to open the file and read it
        System.out.println("Opening file: " + cvsFile);
        try {
            BufferedReader br = new BufferedReader(new FileReader(cvsFile));
            String line = "";
            while ((line = br.readLine()) != null) { // take each line and split it by the separator
                String cvsSeparator = ",";
                String[] lineData = line.split(cvsSeparator); // splitting the line by the separator and store into an array
                dataHandler.handleData(lineData); // send data for processing and counting
            }
            br.close();
        } catch (FileNotFoundException e) { // if file not found
            System.out.println("File not found");
        } catch (IOException e) { // if there is an IO exception
            System.out.println("IO Exception");
        }
    }

    private void printData() { // printer for the data array
        for (String[] strings : data) {
            for (String string : strings) {
                System.out.print(string + " ");
            }
            System.out.println();
        }
    }

}
