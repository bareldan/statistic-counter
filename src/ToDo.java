
// parent class for all types of statistics that can be searched
// contains the statistic type and the abstract methods plusResult and getResult

public abstract class ToDo {
    private final StatisticTypes toDo; // type of statistic

    public ToDo(StatisticTypes toDo) { // constructor
        this.toDo = toDo;
    }

    public abstract void plusResult(Integer value, String from, String to); // abstract method for adding a result

    public abstract Integer getResult();

    public StatisticTypes getToDo() {
        return toDo;
    } // return the type of statistic


    abstract public String toString();

    //to json method

    abstract public String toJson();

    // to xml method
    abstract public String toXML();

}
