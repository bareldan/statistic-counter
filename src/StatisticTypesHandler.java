public class StatisticTypesHandler {


    public static StatisticTypes valueOf(String command) { // converts string to StatisticTypes enum and returns it
        return switch (command) {
            case "CP" -> StatisticTypes.CP;
            case "PP" -> StatisticTypes.PP;
            case "CPD" -> StatisticTypes.CPD;
            case "MD" -> StatisticTypes.MD;
            default -> null;
        };
    }

    public static String toString(StatisticTypes statisticTypes) { // converts StatisticTypes enum to string and returns it
        return switch (statisticTypes) {
            case CP -> "CP - celkovy pocet prujezdu cyklistu";
            case PP -> "PP - pelkovy pocet zaznamenaných prujezdu za den";
            case CPD -> "CPD - celkovy pocet prujezdu cyklistu pro kazdy den";
            case MD -> "MD - den s maximalnim poctem prujezdu";
            default -> null;
        };
    }

    public static boolean checkStatisticType(String statisticTypes) { // checks if string is valid StatisticTypes enum
        return switch (statisticTypes) {
            case "CP" -> true;
            case "PP" -> true;
            case "CPD" -> true;
            case "MD" -> true;
            default -> false;
        };
    }
}
