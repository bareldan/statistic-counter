


// MD - den s maximalnim poctem prujezdu cyklistu
public class MD extends ToDo {
    private Integer result; // result of statistic


    private String currentDate; // result date with max value

    private Integer currentValue; // current value for incrementation

    private String oldDate; // previous date

    private String newDate; // new date

    public MD(StatisticTypes toDo) { // constructor
        super(toDo);
        this.result = 0;
        this.newDate = "";
        this.oldDate = "";
        this.currentValue = 0;
        this.currentDate = "";
    }

    private void setDate(String date) { // set date and result if currentValue is bigger than result
        if (currentValue >= result) {
            result = currentValue;
            currentDate = date;
        }

        currentValue = 0;
        oldDate = newDate;
    }

    public String getDate() {
        return currentDate;
    }

    @Override
    public void plusResult(Integer value, String from, String to) {

        if (this.oldDate.equals("")) oldDate = from;
        if (!from.equals(to)) newDate = to;
        if (newDate.equals(from) && !from.equals(oldDate)) setDate(oldDate);

        currentValue += value;
    }

    public Integer getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "MD " + currentDate + " " + result;
    }


    @Override
    public String toJson() {
        return "{\n" + "\"name\": \"MD - den s maximalnim poctem prujezdu cyklistu\", \n" + "\"date\": " + "\"" + currentDate + "\"" + ",\n\"result\": " + "\"" + result + "\"" + "\n}\n";
    }

    @Override
    public String toXML() {
        return "<name>\n + MD - den s maximalnim poctem prujezdu cyklistu" + "\n</name>\n" + "<value>\n" + "<date>" + currentDate + "\n</date>\n"
                + "<result>\n" + result + "\n</result>\n" + "\n</value>\n";
    }

}
