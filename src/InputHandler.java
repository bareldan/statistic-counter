import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputHandler {

    private String fileName;
    private String[] dataToSearch;

    private String outFile;


    public InputHandler() {
        UserInput();
    }

    private void UserInput() { // takes user input and splits it into an array by spaces
        String input = getInput();
        String[] inputArray = input.split(" ");
        if (inputArray.length == 3) {
            fileName = inputArray[0]; // file name of the file to read
            dataToSearch = inputArray[1].split("-"); // statistic types to search
            outFile = inputArray[2]; // file name of the file to write to
        }
    }

    private String getInput() {
        Scanner scanner = new Scanner(System.in);
        List<String> inputList = new ArrayList<String>();
        String output = "";


        StatisticTypes[] statisticTypes = StatisticTypes.values();
        System.out.println("Please enter the file name, the statistic types and the output file name");
        System.out.println("The statistic types are: ");
        for (StatisticTypes statisticType : statisticTypes) {
            System.out.print(StatisticTypesHandler.toString(statisticType) + '\n');
        }

        while (true) { // takes user input and adds it to a list until the list has 3 elements
            String input = scanner.next();
            inputList.add(input);

            if (inputList.size() == 3) {
                output = inputList.get(0) + " " + inputList.get(1) + " " + inputList.get(2);
                break;
            }
        }

        return output;
    }

    public String getFileName() {
        return fileName;
    }

    public String[] getDataToSearch() {
        return dataToSearch;
    }

    public String getOutFile() {
        return outFile;
    }

}

