
// celkovy pocet prujezdu cyklistu

public class CP extends ToDo {
    private Integer result;

    public CP(StatisticTypes toDo) {
        super(toDo);
        result = 0;
    }


    @Override
    public void plusResult(Integer value, String from, String to) { // add value to result
        result += value;
    }

    public Integer getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "CP " + result;
    }


    @Override
    public String toJson() {
        // make name in ""
        return "{\n" + "\"name\": \"CP - celkovy pocet prujezdu cyklistu\", \n" + "\"result\": " + "\"" + result + "\"" + "\n}\n";
    }

    @Override
    public String toXML() {
        return "<name>\n + CP - celkovy pocet prujezdu cyklistu" + "\n</name>\n" + "<value>\n" + result + "\n</value>\n";
    }


}
