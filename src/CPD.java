import java.util.HashMap;
import java.util.TreeMap;


// CPD - celkovy pocet prujezdu cyklistu pro kazdy den
public class CPD extends ToDo {

    private final TreeMap<String, Integer> result; // Tree Map with date and value of CPD

    private String currentDate;

    private String newDate;

    private Integer currentValue;


    public CPD(StatisticTypes toDo) { // constructor
        super(toDo);
        result = new TreeMap<>();
        this.currentDate = "";
        this.newDate = "";
        currentValue = 0;
    }

    private void setDate(String date) { // put date and value to result treeMap
        result.put(date, currentValue);
        currentValue = 0;
        currentDate = newDate;
    }

    public String getDate() {
        return currentDate;
    }

    @Override
    public void plusResult(Integer value, String from, String to) {


        if (this.currentDate.equals("")) currentDate = from; // takes the first date
        if (!from.equals(to)) newDate = to; // if from and to are not the same, newDate is to
        if (newDate.equals(from) && !from.equals(currentDate))
            setDate(currentDate); // if newDate is from and from is not the same as currentDate, setDate

        currentValue += value; // add value to currentValue
    }

    @Override
    public Integer getResult() {
        return null;
    }

    public Integer getResult(String date) {
        return result.get(date);
    }

    @Override
    public String toString() {
        StringBuilder all = new StringBuilder();
        for (HashMap.Entry<String, Integer> entry : result.entrySet()) {
            all.append(" date: ").append(entry.getKey()).append(" value ").append(entry.getValue()).append("\n");
        }
        return "CPD " + all;
    }

    @Override
    public String toJson() {
        StringBuilder all = new StringBuilder();
        all.append("{\n").append("\"name\": \"CPD - pocet prujezdu cyklistu za den\", \n").append("\"result\": [\n");
        int size = result.size();
        int i = 0;
        for (HashMap.Entry<String, Integer> entry : result.entrySet()) {
            all.append("{\"date\": \"").append(entry.getKey()).append("\", \"value\": \"").append(entry.getValue()).append("\"}\n");
            if (i != size - 1 && size > 1) all.append(",\n");
            i++;
        }
        all.append("]\n}\n");
        return all.toString();
    }

    @Override
    public String toXML() {
        StringBuilder all = new StringBuilder();
        all.append("\n<name>\n + CPD - pocet prujezdu cyklistu za den" + "\n</name>\n");
        for (HashMap.Entry<String, Integer> entry : result.entrySet()) {
            all.append("<date>").append(entry.getKey()).append("</date>").append("<value>").append(entry.getValue()).append("</value>\n");
        }
        return "<CPD>" + all + "</CPD>\n";
    }

}
