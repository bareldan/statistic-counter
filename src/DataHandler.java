import javax.xml.crypto.Data;

public class DataHandler {


    private final Counter counter;


    public DataHandler(StatisticTypes[] toDo) {
        counter = new Counter(toDo);
    }


    // main function of this class, takes the data from the file and sends it to the counter
    public void handleData(String[] data) {

        DaTi from = new DaTi();
        DaTi to = new DaTi();

        int count = 0;
        for (String s : data) {
            if (s.contains("camea")) continue; // skip the first stiring with camera and the second
            else if (DaTi.checkDate(s) && from.getDate().equals("")) { // take the first date and check it
                from.setDate(s);
                if (!counter.getStartDate()) counter.setFirst(from);
            } else if (DaTi.checkDate(s) && to.getDate().equals("")) { // take the second date and check it
                to.setDate(s);
                counter.setLast(to);
            } else if (!DaTi.checkDate(s) && isNumeric(s)) { // take the last string with the number of cyclists
                counter.count(s, from.getDate(), to.getDate());
            } else continue;
            count++;
        }
        if (count != 4) // if there are not 4 strings in the array, send the last amount as 0
            counter.count("0", from.getDate(), to.getDate());

    }

    // check if the string is a number
    private boolean isNumeric(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void getInfo() {
        System.out.println(counter.toString());
    }


    // function for parsing the data to json and xml
    public String toJson() {
        return counter.toJson();
    }

    public String toXml() {
        return counter.toXml();
    }


}
