import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;


//class that represents the date and time of a statistic
public class DaTi {
    private String date;
    private String time;


    public DaTi() {
        this.date = "";
    }

    public DaTi(String date) {
        setDate(date);
    }


    public void setDate(String date) {
        if (!checkDate(date)) return;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        LocalDate localDate = LocalDate.parse(date, formatter);
        this.date = localDate.toString();
    }


    public static boolean checkDate(String date) {
        return checkPattern(date);
    }


    // method that checks if the date is in the right format and returns true if it is and false if it isn't
    public static boolean checkPattern(String input) {
        String pattern = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";
        return Pattern.matches(pattern, input);
    }


    public String getDate() {
        return date;
    }


    public String toString() {
        return date;
    }


}
