
// PP - prumer prujezdu cyklistu za den
public class PP extends ToDo {

    private Integer result; // final result

    private String from; // first date

    private String to; // last date

    public PP(StatisticTypes toDo) {
        super(toDo);
        from = "";
        to = "";
        result = 0;
    }

    @Override
    public void plusResult(Integer value, String from, String to) { // add value to result
        if (this.from.equals("")) this.from = from; // first date
        this.to = to; // last date
        result += value;
    }

    @Override
    public Integer getResult() {
        return countPP();
    }

    private Integer countPP() { // counting the final result value
        long days = Counter.countDays(from, to); // take days between first and last date
        Integer daysCount = 0;
        try {
            daysCount = Math.toIntExact(days); // convert long to int
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage()); // if days is too big
        }
        return result / daysCount; // return final result
    }

    @Override
    public String toString() {
        return "PP " + countPP();
    }


    @Override
    public String toJson() {
        return "{\n" + "\"name\": \"PP - prumer prujezdu cyklistu za den\", \n" + "\"result\": " + "\"" + countPP() + "\"" + "\n}\n";
    }

    @Override
    public String toXML() {
        return "<name>\n + PP - prumer prujezdu cyklistu za den" + "\n</name>\n" + "<value>\n" + countPP() + "\n</value>\n";
    }
}
