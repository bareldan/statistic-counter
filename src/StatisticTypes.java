
// all statistic types that can be searched

public enum StatisticTypes {
    CP, // celkovy pocet prujezdu cyklistu
    PP, // prumerny pocet zaznamenaných prujezdu za den
    CPD, // celkovy pocet prujezdu cyklistu pro kazdy den

    MD, // den s maximalnim poctem prujezdu
}
