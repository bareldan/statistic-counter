import java.util.List;

public class Main {
    public static void main(String[] args) {

        InputHandler inputHandler = new InputHandler(); // class that will take user input

        // array for statistic types that user wants to search
        String[] dataToSearch = inputHandler.getDataToSearch();

        // check if the statistic types are valid and create an array of StatisticTypes
        StatisticTypes[] toDo = new StatisticTypes[dataToSearch.length];
        for (int i = 0; i < dataToSearch.length; i++) {
            if (!StatisticTypesHandler.checkStatisticType(dataToSearch[i])) {
                System.out.println("Error: " + dataToSearch[i] + " is not a valid statistic type");
                return;
            }
            toDo[i] = StatisticTypesHandler.valueOf(dataToSearch[i]);
        }


        // create a DataHandler object that will take data from the file and send it forward
        DataHandler dataHandler = new DataHandler(toDo);

        // create a CVSReader object that will read the file and send the data to the DataHandler
        CVSReader cvsReader = new CVSReader(inputHandler.getFileName(), dataHandler);

        // create an OutputFile object that will write the data to the output file
        OutputFile outputFile = new OutputFile(inputHandler.getOutFile(), dataHandler);
    }
}