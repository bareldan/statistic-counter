import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Counter {

    private final ArrayList<ToDo> dataOut;

    private static DaTi last;

    private static DaTi first;

    public Counter(StatisticTypes[] toDo) {
        dataOut = new ArrayList<>();


        for (StatisticTypes statisticTypes : toDo) {
            if (statisticTypes != null) {
            switch (statisticTypes) {
                case MD -> dataOut.add(new MD(statisticTypes));
                case CP -> dataOut.add(new CP(statisticTypes));
                case PP -> dataOut.add(new PP(statisticTypes));
                case CPD -> dataOut.add(new CPD(statisticTypes));
            }
            }
        }
    }


    public boolean getStartDate(){
        return first != null;
    }

    public boolean getEndDate(){
        return last != null;
    }


    public void setFirst(DaTi first) {
        Counter.first = first;
    }

    public void setLast(DaTi last) {
        Counter.last = last;
    }


    public void count( String s, String from, String to) {
        Integer value = Integer.parseInt(s);

       for (ToDo toDo : dataOut){
              toDo.plusResult(value, from, to);
       }

    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (ToDo toDo : dataOut){
            sb.append(toDo.toString()).append("\n");
        }
        return sb.toString();
    }


    public static long countDays(String from, String to){

        // take the first and the last day and sum the days between them

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime firstDate = LocalDate.parse(from, formatter).atStartOfDay();
        LocalDateTime lastDate = LocalDate.parse(to, formatter).atStartOfDay();

        return ChronoUnit.DAYS.between(firstDate, lastDate);
    }

    public String toJson(){
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("\"from\": \"").append(first.getDate()).append("\",\n");
        sb.append("\"until\": \"").append(last.getDate()).append("\",\n");
        sb.append("\"data\": [\n");
        int size = dataOut.size();
        int i = 0;
        for (ToDo toDo : dataOut){
            sb.append(toDo.toJson());
            if (i < size - 1 && size > 1) sb.append(",\n");
            i++;
        }
        sb.append("]\n");
        sb.append("}\n");
        return sb.toString();
    }


    public String toXml(){
        StringBuilder sb = new StringBuilder();
        sb.append("<report>\n");
        sb.append("<from>").append(first.getDate()).append("</from>\n");
        for (ToDo toDo : dataOut){
            sb.append(toDo.toXML());
        }
        sb.append("<until>").append(last.getDate()).append("</until>\n");
        sb.append("</report>\n");
        return sb.toString();
    }

}
